// Initialize Firebase
var config = {
  apiKey: 'AIzaSyDeVDe4iTTbExlRIkZqLeEvQcydVCtnyFs',
  authDomain: 'blood-mv.firebaseapp.com',
  databaseURL: 'https://blood-mv.firebaseio.com',
  projectId: 'blood-mv',
  storageBucket: 'blood-mv.appspot.com',
  messagingSenderId: '119156820253'
};
firebase.initializeApp(config);

// Initialize waves animation library
Waves.init();

// Create firebase database reference
var db = firebase.firestore();
db.settings({timestampsInSnapshots: true});
var isNewUser = true;

// Enable offline database caching
firebase.firestore().enablePersistence({experimentalTabSynchronization:true}).catch(function(err) { console.log(err); });

// Initialize the spinner
if($('#listPage')[0]) {
  $('#spinner').show();
}

// Add donor details to database
$('.addValue').on('click', function(event) {
  if(! firebase.auth().currentUser) {
    event.preventDefault();
    $('#spinner').hide();
    $('#result').html('Please <a id="logineasy" href=".">Login</a> with a Google account.');
    $('#logineasy').on('click', function(event) {
      event.preventDefault();
      performLogin();
    });
  } else {
    if(contactForm.checkValidity()) {
      event.preventDefault();
      if( $('#first').val() != '' ||
          $('#last').val() != '' ||
          $('#gender').val() != '' ||
          $('#born').val() != '' ||
          $('#group').val() != '' ||
          $('#atoll').val() != '' ||
          $('#island').val() != '' ||
          $('#donated').val() != '' ||
          $('#phone').val() != '' ){
        $('.addValue').attr('disabled', 'disabled');
        $('#result').text('');
        $('#spinner').show();
        if(isNewUser == true) {
          db.collection('donors').add({
            first: $('#first').val(),
            last: $('#last').val(),
            gender: $('#gender').val(),
            born: $('#born').val(),
            group: $('#group').val(),
            atoll: $('#atoll').val(),
            island: $('#island').val(),
            phone: $('#phone').val(),
            donated: $('#donated').val(),
            email: $('#email').val(),
            user: $('#user').val()
          })
          .then(function(docRef) {
            $('#spinner').hide();
            $('#result').text('Record added!');
            contactForm.reset();
          })
          .catch(function(error) {
            $('#spinner').hide();
            $('#result').text('Error: Something went wrong!');
            console.log(error);
          });
        } else {
          db.collection('donors').doc(docId).set({
            first: $('#first').val(),
            last: $('#last').val(),
            gender: $('#gender').val(),
            born: $('#born').val(),
            group: $('#group').val(),
            atoll: $('#atoll').val(),
            island: $('#island').val(),
            phone: $('#phone').val(),
            donated: $('#donated').val(),
            email: $('#email').val(),
            user: $('#user').val()
          })
          .then(function(docRef) {
            $('#spinner').hide();
            $('#result').text('Record updated!');
            contactForm.reset();
          })
          .catch(function(error) {
            $('#spinner').hide();
            $('#result').text('Error: Something went wrong!');
            console.log(error);
          });
        }
        $('.addValue').removeAttr('disabled');
      } else {
        $('#result').text('Please fill all details!');
      }
    }
  }
});

// Add request to database
$('.addRequest').on('click', function(event) {
  if(! firebase.auth().currentUser) {
    event.preventDefault();
    $('#spinner').hide();
    $('#result').html('Please <a id="logineasy" href=".">Login</a> with a Google account.');
    $('#logineasy').on('click', function(event) {
      event.preventDefault();
      performLogin();
    });
  } else {
    if(contactForm.checkValidity()) {
      event.preventDefault();
      if( $('#group').val() != '' ||
          $('#phone').val() != '' ||
          $('#place').val() != '' ){
        $('.addRequest').attr('disabled', 'disabled');
        $('#result').text('');
        $('#spinner').show();
        db.collection('requests').add({
          group: $('#group').val(),
          phone: $('#phone').val(),
          place: $('#place').val(),
          fulfilled: $('#fulfilled').val(),
          datetime: Date.now(),
          email: $('#email').val(),
          user: $('#user').val()
        })
        .then(function(docRef) {
          $('#spinner').hide();
          $('#result').text('Record added!');
          contactForm.reset();
        })
        .catch(function(error) {
          $('#spinner').hide();
          $('#result').text('Error: Something went wrong!');
          console.log(error);
        });
        $('.addValue').removeAttr('disabled');
      } else {
        $('#result').text('Please fill all details!');
      }
    }
  }
});

// Load list of atolls
if($('#addPage')[0]) {
  $.getJSON('atolls.min.json', function(data) {
    var $select = $('#atoll');
    $.each(data, function(index, o) {
      var $option = $('<option/>').attr('value', o.name)
                                  .attr('data-letter', o.letter)
                                  .text(o.name);
      $select.append($option);
    });
  });
}

function loadIslands(value) {
  $('#island').empty()
              .append('<option value="" disabled selected hidden>Select island / ward</option>')
  $.getJSON('islands.min.json', function(data) {
    var $select = $('#island');
    $.each(data, function(index, o) {
      if (o.letter == value) {
        var $option = $('<option/>').attr('value', o.name).text(o.name);
        $select.append($option);
      }
    });
  });
}

$('#atoll').on('change', function(event) {
  loadIslands($(this).find(':selected').data('letter'));
});

// Load donors table
if($('#listPage')[0]) {
  db.collection('donors').get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
      $('#donors').find('tbody').append($('<tr>')
        .append($('<td scope="row">').text(doc.data().first + ' ' + doc.data().last))
        .append($('<td>').text(doc.data().gender))
        .append($('<td>').text(age(doc.data().born)))
        .append($('<td>').text(doc.data().group))
        .append($('<td>').text(doc.data().atoll))
        .append($('<td>').text(doc.data().island))
        .append($('<td>').text(doc.data().phone))
        .append($('<td>').text(formatDate(doc.data().donated, false)))
      )
    });
    $('#spinner').hide();
    $('#donors').DataTable();
  });
}

// Load requests table
if($('#reqListPage')[0]) {
  db.collection('requests').get().then((querySnapshot) => {
    querySnapshot.forEach((doc) => {
      $('#requests').find('tbody').append($('<tr>')
        .append($('<td scope="row">').text(doc.data().group))
        .append($('<td>').text(doc.data().phone))
        .append($('<td>').text(doc.data().place))
        .append($('<td>').text(formatDate(doc.data().datetime, true)))
        .append($('<td>').html('' +
                               (doc.data().user == firebase.auth().getUid() ?
                                 '<i class="material-icons fulf-enabled" id="checkbox-' + doc.id + '" data-fulfilled="' + doc.data().fulfilled + '">' :
                                 '<i class="material-icons fulf-disabled">')
                               + '' +
                               (doc.data().fulfilled == 'true' ?
                                 'check_box</i>' :
                                 'check_box_outline_blank</i>')
                               + ''))
      )
      $('#checkbox-' + doc.id).on('click', function(event) {
        isFulfilled = $('#checkbox-' + doc.id).attr('data-fulfilled');
        ToggleFullfillment(doc.id, isFulfilled);
      });
    });
    $('#spinner').hide();
    $('#requests').DataTable({ "order": [[ 4, "desc" ], [ 3, "asc" ]] });
  })
}

function ToggleFullfillment(docId, isFulfilled) {
  $('#checkbox-' + docId).replaceWith('<i class="fas fa-circle-notch fa-spin" id="checkbox-' + docId + '"></i>');
  if(isFulfilled == 'true') {
    db.collection('requests').doc(docId).update({
      fulfilled: 'false'
    })
    .then(function(docRef) {
      $('#checkbox-' + docId).replaceWith('<i class="material-icons fulf-enabled" id="checkbox-' + docId + '" data-fulfilled="false">check_box_outline_blank</i>');
      $('#checkbox-' + docId).on('click', function(event) {
        isFulfilled = $('#checkbox-' + docId).attr('data-fulfilled');
        ToggleFullfillment(docId, isFulfilled);
      });
    })
    .catch(function(error) {
      $('#checkbox-' + docId).replaceWith('<i class="material-icons fulf-enabled" id="checkbox-' + docId + '" data-fulfilled="true">check_box</i>');
      $('#checkbox-' + docId).on('click', function(event) {
        isFulfilled = $('#checkbox-' + docId).attr('data-fulfilled');
        ToggleFullfillment(docId, isFulfilled);
      });
      console.log(error);
    });
  } else {
    db.collection('requests').doc(docId).update({
      fulfilled: 'true'
    })
    .then(function(docRef) {
      $('#checkbox-' + docId).replaceWith('<i class="material-icons fulf-enabled" id="checkbox-' + docId + '" data-fulfilled="true">check_box</i>');
      $('#checkbox-' + docId).on('click', function(event) {
        isFulfilled = $('#checkbox-' + docId).attr('data-fulfilled');
        ToggleFullfillment(docId, isFulfilled);
      });
    })
    .catch(function(error) {
      $('#checkbox-' + docId).replaceWith('<i class="material-icons fulf-enabled" id="checkbox-' + docId + '" data-fulfilled="false">check_box_outline_blank</i>');
      $('#checkbox-' + docId).on('click', function(event) {
        isFulfilled = $('#checkbox-' + docId).attr('data-fulfilled');
        ToggleFullfillment(docId, isFulfilled);
      });
      console.log(error);
    });
  }
}

function age(dob) {
  dob = new Date(dob);
  var today = new Date();
  var age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
  return age;
}

function formatDate(date, returnTime) {
  date = new Date(date);
  var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  if(returnTime == true) {
    return ('0' + date.getDate()).slice(-2) + '-' + monthNames[date.getMonth()] + '-' + date.getFullYear() + ' '  + ('0' + date.getHours()).slice(-2) + ':' + date.getMinutes();
  } else {
    return ('0' + date.getDate()).slice(-2) + '-' + monthNames[date.getMonth()] + '-' + date.getFullYear();
  }
}

// Initialize Google Auth Provider
var provider = new firebase.auth.GoogleAuthProvider();

$('.btn-login').on('click', function(event) {
  event.preventDefault();
  performLogin();
});

function performLogin() {
  firebase.auth().signInWithRedirect(provider).then(function(result) {
    console.log(result.user);
  }).catch(function(error) {
    console.log(error);
  });
}

function performLogout() {
  firebase.auth().signOut().then(function() {
    $('#nav-req-add').remove();
    $('#nav-add').remove();
    console.log('Signout Succesfull');
  }, function(error) {
    console.log('Signout Failed'); 
    console.log(error);
  });
}

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    $('#display-name').html(user.displayName);
    $('.avatar').replaceWith('<img src="' + user.photoURL + '" class="avatar-img" alt="Avatar" width="100" height="100">');
    db.collection('donors').where('user', '==', firebase.auth().getUid()).limit(1).get().then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        isNewUser = false;
        if($('#addPage')[0]) {
          $('#nav-links').append($('<a id="nav-add" href="/add/">').html('<li class="active waves-effect"><i class="material-icons">edit</i>Edit my details</li>'));
        } else {
          $('#nav-links').append($('<a id="nav-add" href="/add/">').html('<li class="waves-effect"><i class="material-icons">edit</i>Edit my details</li>'));
        }
      });
    })
    .then(function(docRef) {
      if(isNewUser == true) {
        if($('#addPage')[0]) {
          $('#nav-links').append($('<a id="nav-add" href="/add/">').html('<li class="active waves-effect"><i class="material-icons">add</i>Add me as a donor</li>'));
        } else {
          $('#nav-links').append($('<a id="nav-add" href="/add/">').html('<li class="waves-effect"><i class="material-icons">add</i>Add me as a donor</li>'));
        }
      }
      $('#nav-log').remove();
      $('#nav-links').append($('<a id="nav-log" href=""><li class="waves-effect"><i class="material-icons">exit_to_app</i>Logout</li></a>'));
      $('#nav-log').on('click', function(event) {
        event.preventDefault();
        performLogout();
      });
    });
    if($('#reqAddPage')[0]) {
      $('#nav-links').append($('<a id="nav-req-add" href="/request/add/">').html('<li class="active waves-effect"><i class="material-icons">local_hospital</i>Request for Blood</li>'));
    } else {
      $('#nav-links').append($('<a id="nav-req-add" href="/request/add/">').html('<li class="waves-effect"><i class="material-icons">local_hospital</i>Request for Blood</li>'));
    }
    if($('#addPage')[0]) {
      $('#contactForm #email').val(firebase.auth().currentUser.email);
      $('#contactForm #user').val(firebase.auth().getUid());
      // Load existing data if found
      db.collection('donors').where('user', '==', firebase.auth().getUid()).limit(1).get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          docId = doc.id;
          $('.addValue').text('Update Record');
          $('#contactForm #first').val(doc.data().first);
          $('#contactForm #last').val(doc.data().last);
          $('#contactForm #gender').val(doc.data().gender);
          $('#contactForm #born').val(doc.data().born);
          $('#contactForm #group').val(doc.data().group);
          $('#contactForm #atoll').val(doc.data().atoll);
          $.when(loadIslands($('#contactForm #atoll').find(':selected').data('letter'))).done(function(){
            $('#island').append($('<option/>').attr('value', doc.data().island).text(doc.data().island)); // This is just a temporary hack
            $('#contactForm #island').val(doc.data().island);
          });
          $('#contactForm #phone').val(doc.data().phone);
          $('#contactForm #donated').val(doc.data().donated);
        })
      })
      .then(function(docRef) {
        $('#spinner').hide();
      });
    }
    if($('#reqAddPage')[0]) {
      $('#contactForm #email').val(firebase.auth().currentUser.email);
      $('#contactForm #user').val(firebase.auth().getUid());   
      $('#spinner').hide();
    }
  } else {
    $('.headline').html('Please login to add / edit your details');
    $('#display-name').html('Not Logged in');
    $('#nav-log').remove();
    $('#nav-links').append($('<a id="nav-log" href=""><li class="waves-effect"><i class="material-icons">exit_to_app</i>Login</li></a>'));
    $('#nav-log').on('click', function(event) {
      event.preventDefault();
      performLogin();
    });
  }
});

var sidenav = new Sidenav({
  content: document.getElementById('content'),
  sidenav: document.getElementById('sidenav'),
  backdrop: document.getElementById('backdrop')
});

document.getElementById('menu-toggle').addEventListener('click', function() {
  if(sidenav.isOpened) {
    sidenav.close();
  } else {
    sidenav.open();
  }
});

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js').then(function() {
    console.log('[Service Worker] Service worker is all cool.');
  }).catch(function(e) {
    console.error('[Service Worker] Service worker is not so cool.', e);
    throw e;
  });
}
